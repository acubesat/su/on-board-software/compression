## Description

A repository to host code and data regarding the implementation and benchmarking of the compression scheme to be used on-board the AcubeSAT mission to shrink the size of the experimental readout. Here you will currently find `.csv` files; data generated from ESA's [WhiteDwarf](https://essr.esa.int/project/whitedwarf) program. You'll also find some R code to analyze said data, as well as some plots. More to come soon!

---

<div align="center">
<p>
    <a href="https://benchling.com/organizations/acubesat/">Benchling 🎐🧬</a> &bull;
    <a href="https://gitlab.com/acubesat/documentation/cdr-public/-/blob/master/DDJF/DDJF_PL.pdf?expanded=true&viewer=rich">DDJF_PL 📚🧪</a> &bull;
    <a href="https://spacedot.gr/">SpaceDot 🌌🪐</a> &bull;
    <a href="https://acubesat.spacedot.gr/">AcubeSAT 🛰️🌎</a>
</p>
</div>
